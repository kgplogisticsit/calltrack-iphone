//
//  MOPDetailTableViewController.swift
//  Call Track
//
//  Created by Biggs, David on 10/23/15.
//  Copyright © 2015 KGP Logistics. All rights reserved.
//

import UIKit

struct MOPStepData
{
    var stepNumber:String = ""
    var color:String = ""
    var phoneNumbers = [String]()
    var description:String = ""
    var isStepDone:Bool = false
}

class MOPDetailTableViewController: UITableViewController
{
    var mopChangeID:String? = nil       // file name
    var projectID:String? = nil         // project id
    var rowPhoneNumber:String = "xxx"
    var activeRow = 0
    var mopDataList = [MOPStepData]()
    var userID:String? = nil
    var colorAppleBlue = UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)

    @IBOutlet var tblViewDetail: UITableView!

    @IBAction func cancelPhoneCall(segue:UIStoryboardSegue)
    {
    }

    @IBAction func donePhoneCall(segue:UIStoryboardSegue)
    {
        if let phoneCallTableViewController = segue.sourceViewController as? PhoneCallTableViewController
        {
            let phoneOutcomeText = phoneCallTableViewController.phoneOutcomeText!
            //print("Phone call outcome: \(phoneOutcomeText)")

            // Update MOP
            if phoneOutcomeText == "Successfully completed"     // Create global for this string
            {
                mopDataList[activeRow].isStepDone = true
                self.tableView.reloadData()     // Update table with checked box for active row
            }

            // Output Call details
            let locale = NSLocale.currentLocale().localeIdentifier
            let phoneNbr = phoneCallTableViewController.phoneNbr!
            let phoneCallStartUTC = phoneCallTableViewController.phoneCallBeginDate!
            let phoneCallStartLocal = phoneCallStartUTC.descriptionWithLocale(locale)
            let phoneCallDuration = phoneCallTableViewController.phoneCallDuration!
            let phoneCallDurationStr = stringFromTimeInterval(phoneCallDuration)

            //print("Call Start Date (UTC): \(phoneCallStartUTC) UTC time")
            //print("Call Start Date: \(phoneCallStartLocal) local")
            //print("Call Duration: \(phoneCallDuration)")

            //--------- Alert with phone call details ---------
            // Create message strings
            /*let msgStrTitle = "Project \(projectID!): MOP \(mopChangeID!)"

            let msgStrBody = "Step \(mopDataList[activeRow].stepNumber); Call on \(phoneCallStartLocal); Duration = \(phoneCallDurationStr as String); Outcome = \(phoneOutcomeText)"

            let phoneRecAlert = UIAlertView.init(title: msgStrTitle, message: msgStrBody, delegate: nil, cancelButtonTitle: "OK")
            phoneRecAlert.show()*/

            //------------- Create phone record -----------------
            let phoneRecDict = ["userID":userID!, "project": projectID!, "mopID": mopChangeID!, "step": mopDataList[activeRow].stepNumber, "phoneNbr": phoneNbr, "callTime":phoneCallStartLocal, "duration": phoneCallDurationStr as String, "outcome": phoneOutcomeText]

            let responseStr = postPhoneRecord(phoneRecDict)
            print("phone record string = \(responseStr)")

        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()

        //self.tblViewDetail.registerClass(UITableViewCell.self, forCellReuseIdentifier:"DetailCellID")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

        let backButton = UIBarButtonItem(title: "Steps", style: UIBarButtonItemStyle.Done, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton

        //--------- Read file from main bundle TEMP -------
        //let mopURL = NSBundle.mainBundle().URLForResource(mopChangeID!, withExtension: "html")

        // Parse mop file
        //mopDataList = parseMopFileWithURL(mopURL!)

        //---------- New -----------------------
        // Retrieve mop file from Middleware/Project Directory
        print("Retrieving mop file \(mopChangeID!) for project \(projectID!) from middleware")

        let fileStr = getMopFilewithName(mopChangeID!, projID: projectID!)
        //print("mop file str: \(fileStr)")

        mopDataList = parseMopString(fileStr)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    /*override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    // #warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 2
    }*/

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.

        return mopDataList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("DetailCellID", forIndexPath: indexPath)

        //----------------------- Custom Cell ------------------------------

        // Set status image
        var statusImage:UIImage?

        switch mopDataList[indexPath.row].color
        {
        case "Green":
            if mopDataList[indexPath.row].isStepDone == true
            { statusImage = UIImage(named: "checkedGreen") }
            else
            { statusImage = UIImage(named: "uncheckedGreen") }
            break

        case "Yellow":
            if mopDataList[indexPath.row].isStepDone == true
            { statusImage = UIImage(named: "checkedYellow") }
            else
            { statusImage = UIImage(named: "uncheckedYellow") }
            break

        case "Red":
            if mopDataList[indexPath.row].isStepDone == true
            { statusImage = UIImage(named: "checkedRed") }
            else
            { statusImage = UIImage(named: "uncheckedRed") }
            break

        default:
            statusImage = UIImage(named: "uncheckedGreen")
            break
        }

        let imageView = cell.viewWithTag(1) as? UIImageView
        imageView?.image = statusImage

        // Set labels
        var label:UILabel?

        label = cell.viewWithTag(2) as? UILabel
        label?.text = "Step " + mopDataList[indexPath.row].stepNumber

        // Legacy
        //label = cell.viewWithTag(3) as? UILabel
        //label?.text = mopDataList[indexPath.row].description

        // Display number of phone numbers
        let phoneNbrs = mopDataList[indexPath.row].phoneNumbers

        if phoneNbrs.count == 0
        {
            label = cell.viewWithTag(3) as? UILabel
            label?.textColor = UIColor.grayColor()
            label?.text = "No phone numbers"
        }
        else if phoneNbrs.count == 1
        {
            label = cell.viewWithTag(3) as? UILabel
            label?.textColor = colorAppleBlue
            label?.text = "1 phone number"
        }
        else
        {
            label = cell.viewWithTag(3) as? UILabel
            label?.textColor = colorAppleBlue
            label?.text = String(phoneNbrs.count) + " phone numbers"
        }

        return cell
    }

    // NEW...
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        // Perform segue if phone numbers exist
        let phoneNbrs = mopDataList[indexPath.row].phoneNumbers

        if phoneNbrs.count > 0
        {
            activeRow = indexPath.row
            self.performSegueWithIdentifier("seguePhoneCall", sender: self)
        }
    }

    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath)
    {
        print("Accessory btn tapped row \(indexPath.row)")

        // Perform segue
        activeRow = indexPath.row
        self.performSegueWithIdentifier("segueStepDesc", sender: self)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.

        if segue.identifier == "seguePhoneCall"
        {
            let navViewController = segue.destinationViewController as! UINavigationController
            let detailViewController = navViewController.childViewControllers[0] as! PhoneCallTableViewController

            detailViewController.phoneNumbers = mopDataList[activeRow].phoneNumbers
            //print("Prepare for segue with \(phoneNbrs.count) phone numbers")

            /*
            if let cell = sender as? UITableViewCell
            {
                let indexPath = tableView.indexPathForCell(cell)
                //print("index path for selected row = \(indexPath!.row)")

                let phoneNbrs = mopDataList[indexPath!.row].phoneNumbers

                activeRow = indexPath!.row

                let navViewController = segue.destinationViewController as! UINavigationController
                let detailViewController = navViewController.childViewControllers[0] as! PhoneCallTableViewController

                detailViewController.phoneNumbers = phoneNbrs
                print("Prepare for segue with \(phoneNbrs.count) phone numbers")
            }*/
        }
        else if segue.identifier == "segueStepDesc"
        {
            //print("performing segue to Step Desc")

            //let stepDescController = segue.destinationViewController as! StepDescViewController
            //stepDescController.txtViewStepDesc.text = mopDataList[activeRow].description

            if let stepDescController:StepDescViewController = (segue.destinationViewController as! StepDescViewController)
            {
                stepDescController.stepDescText = mopDataList[activeRow].description
            }
        }
    }

    func parseMopString(mopString:String)->[MOPStepData]
    {
        let mopDataStr = mopString as NSString?
        //print("mopDataStr length = \(mopDataStr!.length)")

        // Set up array to store mopdata
        var mopStepDataList = [MOPStepData]()

        // Create regex patterns
        //let regexStepNbr = try! NSRegularExpression(pattern: "rowspan='7'", options: [])
        let regexStepNbr = try! NSRegularExpression(pattern: "rowspan", options: [])
        //let regexColorTags = try! NSRegularExpression(pattern: "#00FF00|#FBB917|#FF0000", options: [])
        let regexColorTags = try! NSRegularExpression(pattern: "#00FF00|#FBB917|#FF0000|#00ff00|#fbb917|#ff0000", options: [])
        //let regexPhoneNbr = try! NSRegularExpression(pattern: "[2-9][0-9][0-9][-| ][0-9][0-9][0-9][-| ][0-9][0-9][0-9][0-9]", options: [])
        let regexPhoneNbr = try! NSRegularExpression(pattern: "[\\(]{0,1}[2-9][0-9][0-9][-|\\s|\\)][\\s]{0,1}[0-9][0-9][0-9][-|\\s][0-9][0-9][0-9][0-9]", options: [])
        let regexHTMLTags = try! NSRegularExpression(pattern: "<[^>]*>", options: [])

        //-----------------------
        // Check for step number matches
        let stepNbrMatches = regexStepNbr.matchesInString(mopDataStr as! String, options: [], range: NSMakeRange(0, mopDataStr!.length))

        let nbrMopSteps = stepNbrMatches.count
        print("nbr of mop steps using rowspan = \(nbrMopSteps)")

        for i in 0..<nbrMopSteps
        {
            //------ Extract step number ------
            var result = stepNbrMatches[i]
            let rowspanStr = mopDataStr?.substringWithRange(NSMakeRange(result.range.location, 35)) // rowspan row
            //print("rowspan str = \(rowspanStr!)")

            let indexLftBracket = rowspanStr?.characters.indexOf(">")   // Requires Swift 2.0
            let indexRgtBracket = rowspanStr?.characters.indexOf("<")
            //print("  lft bracket index = \(indexLftBracket!), rgt bracket index = \(indexRgtBracket!)")

            let stepNbrStr = rowspanStr?.substringWithRange(Range<String.Index>(start: indexLftBracket!.advancedBy(1), end: indexRgtBracket!))
            //print("stepNbr = \(stepNbrStr!)")

            //----- Extract step color ----------
            // Get step start location
            var stepStartLoc = result.range.location + 35

            // Get step location end location (i.e. start of next step)
            var stepEndLoc:Int
            if i < (nbrMopSteps - 1)
            {
                stepEndLoc = stepNbrMatches[i + 1].range.location - 2
            }
            else
            {
                stepEndLoc = mopDataStr!.length - 1
            }

            // Calculate step range
            var stepRange = NSMakeRange(stepStartLoc, stepEndLoc - stepStartLoc)
            //print("  Step Start Loc = \(stepRange.location), Step Range = \(stepRange.length)")

            // Create mop step substring
            var mopStepStr = mopDataStr!.substringWithRange(stepRange)
            //print("\(mopStepStr)")

            var mopStepStrNS = mopStepStr as NSString  // Convert to NSString

            // Check for color tag matches
            let colorTagMatches = regexColorTags.matchesInString(mopStepStrNS as String, options: [], range: NSMakeRange(0, mopStepStrNS.length))
            //print("Nbr of color matches = \(colorTagMatches.count))")

            result = colorTagMatches[0]     // reuse result variable

            let tagHexColor = mopStepStrNS.substringWithRange(NSMakeRange(result.range.location, result.range.length))

            var tagColor:String

            if tagHexColor == "#00FF00"
            { tagColor = "Green" }
            else if tagHexColor == "#FBB917"
            { tagColor = "Yellow" }
            else if tagHexColor == "#FF0000"
            { tagColor = "Red" }
            else if tagHexColor == "#00ff00"
            { tagColor = "Green" }
            else if tagHexColor == "#fbb917"
            { tagColor = "Yellow" }
            else if tagHexColor == "#ff0000"
            { tagColor = "Red" }
            else
            { tagColor = "White" }


            // Create substring
            if tagColor != "White"
            {
                //------ Extract Step Description -------
                //print("Step \(mopStep): Color = \(tagColor), Location = \(result.range.location)")

                stepStartLoc = result.range.location + result.range.length + 2
                stepEndLoc = mopStepStrNS.length - 1

                // Calculate reduced step range
                stepRange = NSMakeRange(stepStartLoc, stepEndLoc - stepStartLoc)
                //print("  Step Start Loc = \(stepRange.location), Step Range = \(stepRange.length)")

                // Update mop step substring
                mopStepStr = mopStepStrNS.substringWithRange(stepRange)
                //print("\(mopStepStr)")

                mopStepStrNS = mopStepStr as NSString  // Convert to NSString

                // Create step description substring
                let stepDescriptionRange = mopStepStrNS.rangeOfString("Step Description")
                let stepSpecificsRange = mopStepStrNS.rangeOfString("Step Specifics")

                let stepDescriptionLocStart = stepDescriptionRange.location + stepDescriptionRange.length
                let stepDescriptionLocEnd = stepSpecificsRange.location

                let stepDescRange = NSMakeRange(stepDescriptionLocStart, stepDescriptionLocEnd - stepDescriptionLocStart)
            let stepDescStr = mopStepStrNS.substringWithRange(stepDescRange) as String
                //print("\(stepDescNSStr)")

                // Replace <BR> with semicolons
            //stepDescStr = stepDescStr.stringByReplacingOccurrencesOfString("<BR>", withString: "; ")
                let stepDescNSStr = stepDescStr as NSString
                //print("\(stepDescNSStr)")

                // Now remove remaining HTML tags
                let stepDescNoHtml = regexHTMLTags.stringByReplacingMatchesInString(stepDescNSStr as String, options: [], range: NSMakeRange(0, stepDescNSStr.length), withTemplate: "")

                //print("\(stepDescNoHtml)")

                //------------ Extract phone numbers -------------
                let stepDescNoHtmlNSStr = stepDescNoHtml as NSString
                let phoneMatches = regexPhoneNbr.matchesInString(stepDescNoHtml, options: [], range: NSMakeRange(0, stepDescNoHtmlNSStr.length))

                //var phoneNbr:String = "Not provided"
                var phoneNbrList = [String]()
                //var stepDescription:String = stepDescNoHtmlNSStr as String
                //var stepDescription2:String = ""

                if phoneMatches.count > 0
                {
                    for phoneResult in phoneMatches
                    {
                        var phoneNbr = stepDescNoHtmlNSStr.substringWithRange(NSMakeRange(phoneResult.range.location, phoneResult.range.length))
                        //print("phone number = \(phoneNbr)")

                        // Clean up phone string
                        phoneNbr = phoneNbr.stringByReplacingOccurrencesOfString("(", withString: "")
                        phoneNbr = phoneNbr.stringByReplacingOccurrencesOfString(")", withString: "-")
                        phoneNbr = phoneNbr.stringByReplacingOccurrencesOfString(" ", withString: "")
                        print("phone nbr = \(phoneNbr)")

                        phoneNbrList.append(phoneNbr)
                    }
                }

                //------------------ TEST ONLY -------------
                /*if phoneMatches.count > 0
                {
                    phoneNbrList[0] = "678-355-6316"        // TEST MY OFFICE NUMBER element 0
                }*/
                //-------------------------------------------

                // Now remove first phone number and trailing space
                //let stepDescription = stepDescNoHtmlNSStr.stringByReplacingOccurrencesOfString(phoneNbrList[0] + " ", withString: "") as String
                //print("\(stepDescription)")

                // REVISED...keep first phone number
                let stepDescription = stepDescNoHtml

                // Trim excess white space and new line returns
                let stepDescription2 = stepDescription.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

                //--------------- Save mop data ------------
                let mopStepData = MOPStepData(stepNumber: stepNbrStr!, color: tagColor, phoneNumbers: phoneNbrList, description: stepDescription2, isStepDone: false)

                // Add data to list
                mopStepDataList.append(mopStepData)
            }
        }

        return mopStepDataList
    }

    func stringFromTimeInterval(interval:NSTimeInterval)->NSString
    {
        let ti = NSInteger(interval)

        //var ms = Int((interval % 1) * 1000)
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)

        return NSString(format: "%0.2d:%0.2d:%0.2d", hours,minutes,seconds)
    }

    func getMopFilewithName(fileName:String, projID:String)->String
    {
        // Create web service URL
        //let baseServicePath = "https://siim-test.bluestreampro.com/iphone/api4/"            // TEST
        ////let baseServicePath = "https://siim.bluestreampro.com/iphone/api4/"               // PROD

        let webServiceEndPointPath = "ProjDocs/GetProjDocFiles.ashx"
        let servicePath = baseServicePath + webServiceEndPointPath
        let serviceURL = NSURL(string: servicePath)
        let theURL = serviceURL!

        // Create URL Request
        let webServiceURLRequest = NSMutableURLRequest(URL: theURL)
        webServiceURLRequest.HTTPMethod = "POST"
        webServiceURLRequest.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        webServiceURLRequest.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")

        let BLUESTREAM_USER_AGENT_TEMPLATE = "BlueStream iOS -TEST- Version/"
        let bundleVersion = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as! String
        let userAgentStr = BLUESTREAM_USER_AGENT_TEMPLATE + bundleVersion
        webServiceURLRequest.setValue(userAgentStr, forHTTPHeaderField: "User-Agent")
        //print("Bundle Version: \(bundleVersion)")

        let siimAuthToken = ""  // Temp
        webServiceURLRequest.setValue(siimAuthToken, forHTTPHeaderField: "X-UserAuth-Token")

        let BLUESTREAM_UNIQUE_DEVICE_ID = UIDevice.currentDevice().identifierForVendor!.UUIDString
        webServiceURLRequest.setValue(BLUESTREAM_UNIQUE_DEVICE_ID, forHTTPHeaderField: "X-UserAuth-DeviceId")
        //println("unique id: \(BLUESTREAM_UNIQUE_DEVICE_ID)")

        webServiceURLRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")

        // Create body data
        var requestNameValuePairs = [String]()

        let webServiceActionName = "getMopFile"

        requestNameValuePairs.append("action=" + webServiceActionName)
        requestNameValuePairs.append("deviceID=" + BLUESTREAM_UNIQUE_DEVICE_ID)
        requestNameValuePairs.append("projID=" + projID)
        requestNameValuePairs.append("mopFileName=" + fileName)

        let requestParametersStr = requestNameValuePairs.joinWithSeparator("&")

        //print("req Para String: \(requestParametersStr)")
        //[action=logIn&deviceID=9684AEB7-73CD-4AAF-95E8-6F2AC9426E04&siimUserID=SM102740&siimPwd=exWahoo77]

        let bodyData = requestParametersStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!

        // Test
        //let bodyDataStr = NSString(data: bodyData, encoding: NSUTF8StringEncoding)!
        //print("request body: \(bodyDataStr)")

        webServiceURLRequest.setValue(String(bodyData.length), forHTTPHeaderField: "Content-Length")
        webServiceURLRequest.HTTPBody = bodyData

        var webServiceResponse:NSURLResponse?
        var webServiceError:NSError?

        let beginLoad = NSDate()

        var webServiceData: NSData?

        do
        {
            webServiceData = try NSURLConnection.sendSynchronousRequest(webServiceURLRequest, returningResponse: &webServiceResponse)
        } catch let error as NSError
        {
            webServiceError = error
            webServiceData = nil
        }

        //---------------------
        let endLoad = NSDate()

        /*NSLog(@"Web service [%@] loaded [%lu] bytes data in [%f] seconds, response [%@], response error [%@]", [[self class] description], (unsigned long)[webServiceData length], [endLoad timeIntervalSinceDate:begin], [webServiceResponse description], [webServiceError localizedDescription]);
        NSString *jsonResponseStringForDebug = [[NSString alloc] initWithData:webServiceData encoding:NSUTF8StringEncoding];
        NSLog(@"Web service class [%@], JSON response [%@]", [[self class] description], jsonResponseStringForDebug);
        [jsonResponseStringForDebug release], jsonResponseStringForDebug = nil;*/

        print("---------- Get Mop file ----------------")
        if webServiceData != nil
        {
            print("Loaded nbr of bytes = \(webServiceData!.length)")
            print("Load time = \(endLoad.timeIntervalSinceDate(beginLoad))")
            print("Response = \(webServiceResponse!.description)")
        }
        //print("Loaded nbr of bytes = \(webServiceData!.length)")
        //print("Load time = \(endLoad.timeIntervalSinceDate(beginLoad))")
        //print("Response = \(webServiceResponse!.description)")
        print("Response Error = \(webServiceError?.localizedDescription)")

        let jsonResponseStringForGetFile = NSString(data: webServiceData!, encoding: NSUTF8StringEncoding)!
        //print("JSON Response for Get Mop File: \(jsonResponseStringForGetFile)")

        // Get substring from json response (allows longer response to be appended)
        //let responseString = jsonResponseStringForGetFile.substringToIndex(16)
        let responseString:String = jsonResponseStringForGetFile as String
        //print("substring = \(responseString)")

        //if jsonResponseStringForGetFile == "Successful logout"
        //if responseString == "Successful logout"
        //{
        //let alert = UIAlertView.init(title: "Logout Result", message: "Successfully logged out", delegate: nil, cancelButtonTitle: "OK")
        //alert.show()
        //}

        return responseString
    }

    func postPhoneRecord(detail:[String:String])->String
    {
        //let phoneRecDict = ["userID":userID!, "project": projectID!, "mopID": mopChangeID!, "step": mopDataList[activeRow].stepNumber, "phoneNbr": phoneNbr!, "callTime":phoneCallStartLocal, "duration": phoneCallDurationStr as String, "outcome": phoneOutcomeText]

        let userID = detail["userID"]!
        let projID = detail["project"]!
        let mopID = detail["mopID"]!
        let step = detail["step"]!
        let phoneNbr = detail["phoneNbr"]!
        let callTime = detail["callTime"]!
        let duration = detail["duration"]!
        let outcome = detail["outcome"]!

        //let responseString = ""
        //let responseString:String = userID + ", " + projID + ", " + mopID + ", " + step + ", " + phoneNbr + ", " + callTime + ", " + duration + ", " + outcome

        //--------------
        // Create web service URL
        //let baseServicePath = "https://siim-test.bluestreampro.com/iphone/api4/"            // TEST
        ////let baseServicePath = "https://siim.bluestreampro.com/iphone/api4/"               // PROD

        let webServiceEndPointPath = "ProjDocs/GetProjDocFiles.ashx"
        let servicePath = baseServicePath + webServiceEndPointPath
        let serviceURL = NSURL(string: servicePath)
        let theURL = serviceURL!

        // Create URL Request
        let webServiceURLRequest = NSMutableURLRequest(URL: theURL)
        webServiceURLRequest.HTTPMethod = "POST"
        webServiceURLRequest.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        webServiceURLRequest.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")

        let BLUESTREAM_USER_AGENT_TEMPLATE = "BlueStream iOS -TEST- Version/"
        let bundleVersion = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as! String
        let userAgentStr = BLUESTREAM_USER_AGENT_TEMPLATE + bundleVersion
        webServiceURLRequest.setValue(userAgentStr, forHTTPHeaderField: "User-Agent")
        //print("Bundle Version: \(bundleVersion)")

        let siimAuthToken = ""  // Temp
        webServiceURLRequest.setValue(siimAuthToken, forHTTPHeaderField: "X-UserAuth-Token")

        let BLUESTREAM_UNIQUE_DEVICE_ID = UIDevice.currentDevice().identifierForVendor!.UUIDString
        webServiceURLRequest.setValue(BLUESTREAM_UNIQUE_DEVICE_ID, forHTTPHeaderField: "X-UserAuth-DeviceId")
        //println("unique id: \(BLUESTREAM_UNIQUE_DEVICE_ID)")

        webServiceURLRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")

        // Create body data
        var requestNameValuePairs = [String]()

        let webServiceActionName = "createPhoneLog"

        requestNameValuePairs.append("action=" + webServiceActionName)
        //requestNameValuePairs.append("deviceID=" + BLUESTREAM_UNIQUE_DEVICE_ID)

        requestNameValuePairs.append("userID=" + userID)
        requestNameValuePairs.append("projID=" + projID)
        requestNameValuePairs.append("mopID=" + mopID)
        requestNameValuePairs.append("step=" + step)
        requestNameValuePairs.append("phoneNbr=" + phoneNbr)
        requestNameValuePairs.append("callTime=" + callTime)
        requestNameValuePairs.append("duration=" + duration)
        requestNameValuePairs.append("outcome=" + outcome)

        let requestParametersStr = requestNameValuePairs.joinWithSeparator("&")
        //print("req Para String: \(requestParametersStr)")
        //[action=logIn&deviceID=9684AEB7-73CD-4AAF-95E8-6F2AC9426E04&siimUserID=SM102740&siimPwd=exWahoo77]

        let bodyData = requestParametersStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!

        // Test ---
        //let bodyDataStr = NSString(data: bodyData, encoding: NSUTF8StringEncoding)!
        //print("request body: \(bodyDataStr as String)")
        //---------

        webServiceURLRequest.setValue(String(bodyData.length), forHTTPHeaderField: "Content-Length")
        webServiceURLRequest.HTTPBody = bodyData

        var webServiceResponse:NSURLResponse?
        var webServiceError:NSError?

        let beginLoad = NSDate()

        var webServiceData: NSData?

        do
        {
            webServiceData = try NSURLConnection.sendSynchronousRequest(webServiceURLRequest, returningResponse: &webServiceResponse)
        } catch let error as NSError
        {
            webServiceError = error
            webServiceData = nil
        }

        //---------------------
        let endLoad = NSDate()

        /*NSLog(@"Web service [%@] loaded [%lu] bytes data in [%f] seconds, response [%@], response error [%@]", [[self class] description], (unsigned long)[webServiceData length], [endLoad timeIntervalSinceDate:begin], [webServiceResponse description], [webServiceError localizedDescription]);
        NSString *jsonResponseStringForDebug = [[NSString alloc] initWithData:webServiceData encoding:NSUTF8StringEncoding];
        NSLog(@"Web service class [%@], JSON response [%@]", [[self class] description], jsonResponseStringForDebug);
        [jsonResponseStringForDebug release], jsonResponseStringForDebug = nil;*/

        print("---------- Send phone record event log ----------------")
        if webServiceData != nil
        {
            print("Loaded nbr of bytes = \(webServiceData!.length)")
            print("Load time = \(endLoad.timeIntervalSinceDate(beginLoad))")
            print("Response = \(webServiceResponse!.description)")
        }
        //print("Loaded nbr of bytes = \(webServiceData!.length)")
        //print("Load time = \(endLoad.timeIntervalSinceDate(beginLoad))")
        //print("Response = \(webServiceResponse!.description)")
        print("Response Error = \(webServiceError?.localizedDescription)")

        let jsonResponseStringForPhoneLog = NSString(data: webServiceData!, encoding: NSUTF8StringEncoding)!
        //print("JSON Response for Send Phone Log: \(jsonResponseStringForPhoneLog)")

        // Get substring from json response (allows longer response to be appended)
        //let responseString = jsonResponseStringForPhoneLog.substringToIndex(16)
        let responseString:String = jsonResponseStringForPhoneLog as String
        //print("substring = \(responseString)")

        //if jsonResponseStringForPhoneLog == "Successful logout"
        //if responseString == "Successful logout"
        //{
        //let alert = UIAlertView.init(title: "Logout Result", message: "Successfully logged out", delegate: nil, cancelButtonTitle: "OK")
        //alert.show()
        //}
        
        return responseString
    }
}

