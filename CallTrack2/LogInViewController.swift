//
//  LogInViewController.swift
//  Call Track
//
//  Created by Biggs, David on 10/23/15.
//  Copyright © 2015 KGP Logistics. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController
{
    @IBOutlet weak var viewLoginPanel: UIView!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtUserPassword: UITextField!
    @IBOutlet weak var actIndLogIn: UIActivityIndicatorView!

    var userID:String?

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Round corners of log in panel
        self.viewLoginPanel.layer.cornerRadius = 5
        self.viewLoginPanel.layer.masksToBounds = true

        txtUserName.addTarget(self, action: "onChangeUserName", forControlEvents: UIControlEvents.EditingDidEndOnExit)
        txtUserPassword.addTarget(self, action: "onChangePassword", forControlEvents: UIControlEvents.EditingDidEndOnExit)

        // Network creds        TEMP
        //txtUserName.text = "hgutti"
        //txtUserPassword.text = "Harikha0702!"

        //txtUserName.text = "dbiggs"
        //txtUserPassword.text = "exWahoo55"

        userID = txtUserName.text
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */

    func onChangeUserName()
    {
        txtUserPassword.becomeFirstResponder()  
    }

    func onChangePassword()
    {
        print("Ready to log in")
    }

    @IBAction func txtUserNameChanged(sender: UITextField)
    {
        txtUserName.text = sender.text

        userID = txtUserName.text
    }

    @IBAction func txtPasswordChanged(sender: UITextField)
    {
        txtUserPassword.text = sender.text
    }

    @IBAction func btnLogInTapped(sender: UIButton)
    {
        if txtUserName.text != "" && txtUserPassword.text != ""
        {
            authenticateUser()
        }
        else
        {
            print("Missing user name or password")

            // Display alert message
            let logInAlert = UIAlertView(title: "Authentication Error", message: "Missing username and/or password", delegate: nil, cancelButtonTitle: "Ok")
            logInAlert.show()
        }

        //--------------------------- Legacy --------------------------------
        /*
        // Start Activity indicator
        self.actIndLogIn.hidden = false;
        self.actIndLogIn.startAnimating()

        // Create web service URL
        //let baseServicePath = "https://siim-test.bluestreampro.com/iphone/api4/"            // TEST
        ////let baseServicePath = "https://siim.bluestreampro.com/iphone/api4/"               // PROD

        let webServiceEndPointPath = "Auth.ashx"
        let servicePath = baseServicePath + webServiceEndPointPath
        let serviceURL = NSURL(string: servicePath)
        let theURL = serviceURL!

        // Create URL Request
        let webServiceURLRequest = NSMutableURLRequest(URL: theURL)
        webServiceURLRequest.HTTPMethod = "POST"
        webServiceURLRequest.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        webServiceURLRequest.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")

        let BLUESTREAM_USER_AGENT_TEMPLATE = "BlueStream iOS -TEST- Version/"
        let bundleVersion = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as! String
        let userAgentStr = BLUESTREAM_USER_AGENT_TEMPLATE + bundleVersion
        webServiceURLRequest.setValue(userAgentStr, forHTTPHeaderField: "User-Agent")
        //print("Bundle Version: \(bundleVersion)")

        let siimAuthToken = ""  // Temp
        webServiceURLRequest.setValue(siimAuthToken, forHTTPHeaderField: "X-UserAuth-Token")

        let BLUESTREAM_UNIQUE_DEVICE_ID = UIDevice.currentDevice().identifierForVendor!.UUIDString
        webServiceURLRequest.setValue(BLUESTREAM_UNIQUE_DEVICE_ID, forHTTPHeaderField: "X-UserAuth-DeviceId")
        //println("unique id: \(BLUESTREAM_UNIQUE_DEVICE_ID)")

        webServiceURLRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")

        //-------------------------- LOG IN ------------------------------
        // Create body data
        var requestNameValuePairs = [String]()

        //var webServiceActionName = "logIn"
        let webServiceActionName = "logInLDAP"

        let siimUserID = txtUserName.text!
        let siimPassword = txtUserPassword.text!

        requestNameValuePairs.append("action=" + webServiceActionName)
        requestNameValuePairs.append("deviceID=" + BLUESTREAM_UNIQUE_DEVICE_ID)
        requestNameValuePairs.append("siimUserID=" + siimUserID)
        requestNameValuePairs.append("siimPwd=" + siimPassword)

        let requestParametersStr = requestNameValuePairs.joinWithSeparator("&")

        //print("req Para String: \(requestParametersStr)")
        //[action=logIn&deviceID=9684AEB7-73CD-4AAF-95E8-6F2AC9426E04&siimUserID=SM102740&siimPwd=exWahoo77]

        let bodyData = requestParametersStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!

        // Test
        //let bodyDataStr = NSString(data: bodyData, encoding: NSUTF8StringEncoding)!
        //print("request body: \(bodyDataStr)")
        //[action=logIn&deviceID=9684AEB7-73CD-4AAF-95E8-6F2AC9426E04&siimUserID=SM102740&siimPwd=exWahoo77]

        webServiceURLRequest.setValue(String(bodyData.length), forHTTPHeaderField: "Content-Length")
        webServiceURLRequest.HTTPBody = bodyData

        //var webServiceResponse:NSURLResponse?
        //var webServiceError:NSError?

        let beginLoad = NSDate()

        //var webServiceData: NSData?

        let queue:NSOperationQueue = NSOperationQueue()

        NSURLConnection.sendAsynchronousRequest(webServiceURLRequest, queue: queue, completionHandler:
            { (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in

                let httpResponse = response as? NSHTTPURLResponse
                print("http response = \(httpResponse!.statusCode)")

                if httpResponse!.statusCode == 200
                {
                    let jsonResponseStringForLogIn = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                    print("JSON Response for LogIn: \(jsonResponseStringForLogIn)")

                    if jsonResponseStringForLogIn == "Successful login"
                    {
                        let endLoad = NSDate()
                        print("Load time = \(endLoad.timeIntervalSinceDate(beginLoad))")

                        self.actIndLogIn.stopAnimating()
                        self.performSegueWithIdentifier("unwindLogIn", sender: self)
                    }
                    else //if error != nil
                    {
                        // Kill http process
                        NSURLConnection.cancelPreviousPerformRequestsWithTarget(self)
                        self.actIndLogIn.stopAnimating()

                        // Display alert: Log in error
                        let logInAlert = UIAlertView(title: "Authentication Error", message: "Unable to Log In. Verify username and password", delegate: nil, cancelButtonTitle: "Ok")
                        logInAlert.show()
                    }
                }
                else
                {
                    print("Server Error: Return code \(httpResponse!.statusCode)")
                }
        })*/
    }

    func authenticateUser()
    {
        let userName = txtUserName.text!
        let userPassword = txtUserPassword.text!

        let BLUESTREAM_UNIQUE_DEVICE_ID = UIDevice.currentDevice().identifierForVendor!.UUIDString

        print("Authenticating user: \(userName) with password: \(userPassword)" )

        // Start Activity indicator
        self.actIndLogIn.startAnimating()

        // Create web service URL
        //let baseServicePath1 = "https://siim-test.bluestreampro.com/iphone/api4/"            // TEST
        let baseServicePath1 = "https://siim-test.bluestreampro.com/CallTrack/api/"
        //let baseServicePath1 = "https://siim-test.bluestreampro.com/AssetTrack/api/"

        //let webServiceEndPointPath = "Auth.ashx"
        let webServiceEndPointPath = "ServiceHandler.ashx"

        let request = NSMutableURLRequest(URL: NSURL(string: baseServicePath1 + webServiceEndPointPath)!)
        let session = NSURLSession.sharedSession()

        //--------------------- Create body data ---------------------------
        var requestNameValuePairs = [String]()

        let webServiceActionName = "logInLDAP"

        requestNameValuePairs.append("action=" + webServiceActionName)
        requestNameValuePairs.append("deviceID=" + BLUESTREAM_UNIQUE_DEVICE_ID)
        requestNameValuePairs.append("siimUserID=" + userName)
        requestNameValuePairs.append("siimPwd=" + userPassword)

        let requestParametersStr = requestNameValuePairs.joinWithSeparator("&")
        //print("req Para String: \(requestParametersStr)")

        let bodyData = requestParametersStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!

        request.setValue(String(bodyData.length), forHTTPHeaderField: "Content-Length")
        request.HTTPBody = bodyData
        request.HTTPMethod = "POST"

        //var isLoggedIn:Bool = false

        //--------------------- Create data task -------------------------
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in

            if error == nil
            {
                print("Response: \(response!)")

                let httpResp = response as! NSHTTPURLResponse

                if httpResp.statusCode == 200
                {
                    let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("Data string: \(strData!)")

                    if strData! == "Successful login"
                    {
                        print("Authentication was successful")

                        //isLoggedIn = true

                        // Turn off activity indicator
                        //dispatch_async(dispatch_get_main_queue(), {self.actIndLogIn.stopAnimating()})

                        // Unwind and return to Asset List Tableview screen
                        //self.performSegueWithIdentifier("unwindLogin", sender: self)

                        dispatch_async(dispatch_get_main_queue(), {self.actIndLogIn.stopAnimating(); self.performSegueWithIdentifier("unwindLogIn", sender: self)})
                    }
                    else
                    {
                        print("Authentication failed")

                        // Turn off activity indicator
                        dispatch_async(dispatch_get_main_queue(), {self.actIndLogIn.stopAnimating()})

                        // Display alert
                        dispatch_async(dispatch_get_main_queue(), {let logInAlert = UIAlertView(title: "Authentication Error", message: "Incorrect username and/or password", delegate: nil, cancelButtonTitle: "Ok")
                            logInAlert.show()})
                    }
                }
                else
                {
                    print("Server response error: HTTP response status code = \(httpResp.statusCode)")

                    let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("Data string: \(strData!)")

                    // Turn off activity indicator
                    dispatch_async(dispatch_get_main_queue(), {self.actIndLogIn.stopAnimating()})

                    // Display alert
                    dispatch_async(dispatch_get_main_queue(), {let logInAlert = UIAlertView(title: "Server Response Error", message: "Unable to authenticate, HTTP response code: \(httpResp.statusCode)", delegate: nil, cancelButtonTitle: "Ok")
                        logInAlert.show()})
                }
            }
            else
            {
                print("Server connection error: \(error)")

                // Turn off activity indicator
                dispatch_async(dispatch_get_main_queue(), {self.actIndLogIn.stopAnimating()})

                // Display alert
                dispatch_async(dispatch_get_main_queue(), {let logInAlert = UIAlertView(title: "Server Connection Error", message: "Unable to connect, Error: \(error)", delegate: nil, cancelButtonTitle: "Ok")
                    logInAlert.show()})
            }
        })
        
        task.resume()
    }

/*
    func logIn()
    {
        print("Logging in...")

        let webServiceEndPointPath = "Auth.ashx"

        let request = NSMutableURLRequest(URL: NSURL(string: baseServicePath + webServiceEndPointPath)!)
        let session = NSURLSession.sharedSession()
        //request.HTTPMethod = "POST"

        //--------------------- Create body data ---------------------------
        var requestNameValuePairs = [String]()

        let webServiceActionName = "logInLDAP"

        let siimUserID = txtUserName.text!
        let siimPassword = txtUserPassword.text!

        requestNameValuePairs.append("action=" + webServiceActionName)
        //requestNameValuePairs.append("deviceID=" + BLUESTREAM_UNIQUE_DEVICE_ID)
        requestNameValuePairs.append("siimUserID=" + siimUserID)
        requestNameValuePairs.append("siimPwd=" + siimPassword)

        let requestParametersStr = requestNameValuePairs.joinWithSeparator("&")
        print("req Para String: \(requestParametersStr)")

        let bodyData = requestParametersStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!

        request.setValue(String(bodyData.length), forHTTPHeaderField: "Content-Length")
        request.HTTPBody = bodyData
        request.HTTPMethod = "POST"

        //--------------------- Create data task -------------------------
        var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            print("Response: \(response)")
            var strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
            print("Body: \(strData)")
            //var err:NSError?

            do
            {
                var json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary

                /*if let parseJSON = json
                {
                    var success = parseJSON["success"] as? Int
                    print("Success: \(success)")
                }
                else
                {
                    let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("Error1 could not parse JSON: \(jsonStr)")
                }*/
            }
            catch
            {
                print("Error2 could not parse JSON")
            }

            })

        task.resume()
    }*/
}
