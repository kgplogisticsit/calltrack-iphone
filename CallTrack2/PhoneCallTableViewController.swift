//
//  PhoneCallTableViewController.swift
//  Call Track
//
//  Created by Biggs, David on 10/23/15.
//  Copyright © 2015 KGP Logistics. All rights reserved.
//

import UIKit

class PhoneCallTableViewController: UITableViewController
{
    var phoneNumbers = [String]()       // List of phone numbers
    var phoneNbr:String? = nil          // Actual number called
    var phoneOutcomeSelectedRow:Int? = nil
    var phoneOutcomeText:String? = nil
    var wasPhoneButtonTapped:Bool = false
    var phoneCallBeginDate:NSDate? = nil
    var phoneCallDuration:NSTimeInterval? = nil

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

        // Registration not needed
        //self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "idCellPhoneNumber")
        //self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "idCellCallOutcome")

        // Init phone numbers TEMP
        //phoneNumbers = ["678-355-6316","770-432-1956"]

        // Disable nav bar "Done" button until outcome is selected
        self.navigationItem.rightBarButtonItem?.enabled = false
    }

    ///////////////////////////
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)

        // Add observers
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "willResignActive", name: UIApplicationWillResignActiveNotification, object: nil)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didBecomeActive", name: UIApplicationDidBecomeActiveNotification, object: nil)
    }

    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(true)

        // Remove observers
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationWillResignActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
    }

    func willResignActive()
    {
        //print("Will resign active now")

        if wasPhoneButtonTapped == true
        { phoneCallBeginDate = NSDate() }
    }

    func didBecomeActive()
    {
        //print("Did become active")

        if wasPhoneButtonTapped == true
        {
            let endCallDate = NSDate()
            phoneCallDuration = endCallDate.timeIntervalSinceDate(phoneCallBeginDate!)

            //print("Phone call duration = \(phoneCallDuration!) sec")
        }
    }
    ////////////////////////

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        // Return number of sections in table
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // Return number of rows in each section
        if section == 0
        {
            return phoneNumbers.count
        }
        else
        {
            return 5
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        // Configure the cells
        var cell:UITableViewCell

        if indexPath.section == 0
        {
            cell = tableView.dequeueReusableCellWithIdentifier("idCellPhoneNumber", forIndexPath: indexPath)
            cell.textLabel!.text = phoneNumbers[indexPath.row]
        }
        else    // section = 1
        {
            cell = tableView.dequeueReusableCellWithIdentifier("idCellCallOutcome", forIndexPath: indexPath)

            var textLabelStr:String

            switch indexPath.row
            {
            case 0:
                textLabelStr = "Successfully completed"

            case 1:
                textLabelStr = "Wrong number"

            case 2:
                textLabelStr = "No answer"

            case 3:
                textLabelStr = "Caller hung up"

            case 4:
                textLabelStr = "Lost connection"

            default:
                textLabelStr = " "

            }

            cell.textLabel!.text = textLabelStr

        }

        return cell
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        var sectionTitle:String

        if section == 0
        {
            sectionTitle = "CHOOSE AT&T PHONE NUMBER TO CALL"
        }
        else
        {
            sectionTitle = "CALL OUTCOME (SELECT ONE)"
        }

        return sectionTitle
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if indexPath.section == 0
        {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)

            // Phone button tapped
            wasPhoneButtonTapped = true
            self.navigationItem.leftBarButtonItem?.enabled = false

            //------- Make phone call using "tel" or "telprompt" (not documented) and return to app after done ------

            // Create phone number URL
            phoneNbr = phoneNumbers[indexPath.row]

            // Clean up phone string
            /*phoneNbr = phoneNbr!.stringByReplacingOccurrencesOfString("(", withString: "")
            phoneNbr = phoneNbr!.stringByReplacingOccurrencesOfString(")", withString: "-")
            phoneNbr = phoneNbr!.stringByReplacingOccurrencesOfString(" ", withString: "")*/
            print("phone nbr = \(phoneNbr!)")

            let phoneNumberURL = NSURL(string: "tel://" + phoneNbr!)

            if UIApplication.sharedApplication().canOpenURL(phoneNumberURL!)
            {
                // Make the call
                UIApplication.sharedApplication().openURL(phoneNumberURL!)
            }
            else
            {
                wasPhoneButtonTapped = false
                self.navigationItem.leftBarButtonItem?.enabled = true

                // Display Error alert message
                let alert = UIAlertView.init(title: "Phone Call Error", message: "Call services not available", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }

        }
        else    // section = 1
        {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)

            if wasPhoneButtonTapped == true
            {
                // Create indexpath for current selection
                if phoneOutcomeSelectedRow != nil
                {
                    // Disable checkmark for current selected row
                    let index = NSIndexPath(forRow: phoneOutcomeSelectedRow!, inSection: 1)

                    let cell0 = tableView.cellForRowAtIndexPath(index)
                    cell0?.accessoryType = UITableViewCellAccessoryType.None
                }

                // Enable checkmark for new selected row
                let cell1 = tableView.cellForRowAtIndexPath(indexPath)
                cell1?.accessoryType = UITableViewCellAccessoryType.Checkmark

                // Update current selection
                phoneOutcomeSelectedRow = indexPath.row

                // Get text for outcome
                phoneOutcomeText = cell1?.textLabel?.text
                //print("Text outcome: \(phoneOutcomeText!)")

                // Enable "Done" nav bar button
                self.navigationItem.rightBarButtonItem?.enabled = true
            }
        }
    }    
}
