//
//  SelectMOPTableViewController.swift
//  Call Track
//
//  Created by Biggs, David on 10/23/15.
//  Copyright © 2015 KGP Logistics. All rights reserved.
//

import UIKit

// Create web service URL
//let baseServicePath = "https://siim-test.bluestreampro.com/iphone/api4/"            // TEST
let baseServicePath = "https://siim.bluestreampro.com/iphone/api4/"               // PROD

class SelectMOPTableViewController: UITableViewController,UIAlertViewDelegate,UISearchBarDelegate
{
    let versionNbr:String = "Version 1.0.9"

    var items = [String]()
    var projectID = ""
    var isLoggedIn = false
    var userID:String = "Not provided"

    @IBOutlet weak var barBtnLogInOut: UIBarButtonItem!
    @IBOutlet var tblViewMOPList: UITableView!
    @IBOutlet weak var searchBarProjectID: UISearchBar!

    @IBAction func cancelLogIn(segue:UIStoryboardSegue)
    {
    }

    @IBAction func doneLogIn(segue:UIStoryboardSegue)
    {
        // Log in was successful, change bar button label to "log out"
        isLoggedIn = true

        barBtnLogInOut.title = "Log out"

        if let loginTableViewController = segue.sourceViewController as? LogInViewController
        {
            userID = loginTableViewController.userID!
        }
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()

        if isLoggedIn == false
        {
            barBtnLogInOut.title = "Log in"

            self.performSegueWithIdentifier("segueLogIn", sender: self)
        }
        else
        {
            barBtnLogInOut.title = "Log out"
        }

        searchBarProjectID.delegate = self

        let backButton = UIBarButtonItem(title: "MOP Search", style: UIBarButtonItemStyle.Done, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return items.count
    }

    // Needed for version prior to iOS8
    /*override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {

    }*/

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if items.count > 0
        {
            return "Choose MOP"
        }
        else
        {
            return ""
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath)

        // Strip off file extension
        let fileStr = items[indexPath.row].characters.split{$0 == "."}.map(String.init)

        // Configure the cell
        cell.textLabel!.text = fileStr[0]

        //cell.textLabel!.text = items[indexPath.row]
        //cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator

        return cell
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.

        if segue.identifier == "segueMopDetail"
        {
            let mopDetailViewController = segue.destinationViewController as! MOPDetailTableViewController

            let indexPath = self.tblViewMOPList.indexPathForSelectedRow!
            //let destinationTitle = String(items[indexPath.row])     // Name for this row
            let destinationTitle = "MOP Steps"

            mopDetailViewController.title = destinationTitle
            mopDetailViewController.mopChangeID = items[indexPath.row]
            mopDetailViewController.projectID = projectID
            mopDetailViewController.userID = userID
        }
    }

    // Required search bar delegate to be assigned to self
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        //print("search text: \(searchBar.text!)")
        let projIDStr:String = searchBar.text!
        print("Project ID to search: \(projIDStr)");

        // Dismiss keyboard by ending first responder status
        //searchBar.endEditing(true)

        var firstChar = projIDStr.substringToIndex(projIDStr.startIndex.advancedBy(1))

        if firstChar != "S"
        {
            // Display alert: Incorrect first character for project
            let searchAlert = UIAlertView(title: "Project ID Error", message: "First character must be 'S'", delegate: nil, cancelButtonTitle: "Ok")
            searchAlert.show()
        }
        else
        {
            // Init search results array
            var searchResult = [String]()

            // Search for project and get result
            let resultStr = getFileNamesForProjID(projIDStr)
            print("resultStr = \(resultStr)")

            if resultStr != ""
            {
                firstChar = resultStr.substringToIndex(resultStr.startIndex.advancedBy(1))
            }

            if firstChar == "<"
            {
                // Display alert: Project not found
                let noProjectAlert = UIAlertView(title: "Project not found", message: "Unable to locate Project \(searchBar.text!)", delegate: nil, cancelButtonTitle: "Ok")
                noProjectAlert.show()
            }
            else
            {
                // Create list from result string
                searchResult = resultStr.characters.split{$0 == ","}.map(String.init)

                if isLoggedIn == true
                {
                    if searchResult.count > 0
                    {
                        if searchResult[0] == "Project not found"
                        {
                            // Display alert: Project not found
                            let noProjectAlert = UIAlertView(title: "Project not found", message: "Unable to locate Project \(searchBar.text!)", delegate: nil, cancelButtonTitle: "Ok")
                            noProjectAlert.show()
                        }
                        else
                        {
                            // Dismiss keyboard by ending first responder status
                            searchBar.endEditing(true)

                            // Add mop files to list
                            for mopFile in searchResult
                            {
                                // Add file to items list
                                items.append(mopFile)
                            }

                            // Save project id
                            projectID = searchBar.text!

                            self.tblViewMOPList.reloadData()
                        }
                    }
                    else
                    {
                        // Display alert: No mop files found for project
                        let noFilesAlert = UIAlertView(title: "Files not found", message: "No MOP files were found for Project \(searchBar.text!)", delegate: nil, cancelButtonTitle: "Ok")
                        noFilesAlert.show()
                    }
                }
                else
                {
                    // Display alert: Not logged in
                    let notLoggedInAlert = UIAlertView(title: "Authentication Error", message: "You must log in to perform search", delegate: nil, cancelButtonTitle: "Ok")
                    notLoggedInAlert.show()
                }
            }
        }
    }

    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if !searchBar.isFirstResponder()
        {
            //user tapped clear button
            items.removeAll()
            self.tblViewMOPList.reloadData()
        }
    }

    @IBAction func barBtnAboutTapped(sender: UIBarButtonItem)
    {
        let aboutAlert = UIAlertView(title: "BlueStream Call Track", message: versionNbr, delegate: nil, cancelButtonTitle: "OK")
        aboutAlert.show()
    }

    @IBAction func barBtnLogInOut(sender: UIBarButtonItem)
    {
        if isLoggedIn == false
        {
            self.performSegueWithIdentifier("segueLogIn", sender: self)
        }
        else
        {
            // Perform log out
            print("Logging out...")

            logOut()
            isLoggedIn = false
            barBtnLogInOut.title = "Log in"

            searchBarProjectID.text = ""

            items.removeAll()
            tblViewMOPList.reloadData()
        }

        /*let loginAlert = UIAlertView(title: "Login", message: "Enter KGP Domain ID & Password", delegate: self, cancelButtonTitle: "Forgot Password", otherButtonTitles: "Login")
        loginAlert.alertViewStyle = UIAlertViewStyle.LoginAndPasswordInput
        loginAlert.show()*/
    }

    func logOut()
    {

    }

    func getFileNamesForProjID(projID:String)->String
    {
        // Create web service URL
        //let baseServicePath = "https://siim-test.bluestreampro.com/iphone/api4/"            // TEST
        ////let baseServicePath = "https://siim.bluestreampro.com/iphone/api4/"               // PROD

        //let webServiceEndPointPath = "ProjectDocs/GetProjDocFiles.ashx"
        let webServiceEndPointPath = "ProjDocs/GetProjDocFiles.ashx"

        let servicePath = baseServicePath + webServiceEndPointPath
        let serviceURL = NSURL(string: servicePath)
        let theURL = serviceURL!

        // Create URL Request
        let webServiceURLRequest = NSMutableURLRequest(URL: theURL)
        webServiceURLRequest.HTTPMethod = "POST"
        webServiceURLRequest.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        webServiceURLRequest.setValue("gzip", forHTTPHeaderField: "Accept-Encoding")

        let BLUESTREAM_USER_AGENT_TEMPLATE = "BlueStream iOS -TEST- Version/"
        let bundleVersion = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as! String
        let userAgentStr = BLUESTREAM_USER_AGENT_TEMPLATE + bundleVersion
        webServiceURLRequest.setValue(userAgentStr, forHTTPHeaderField: "User-Agent")
        //print("Bundle Version: \(bundleVersion)")

        let siimAuthToken = ""  // Temp
        webServiceURLRequest.setValue(siimAuthToken, forHTTPHeaderField: "X-UserAuth-Token")

        let BLUESTREAM_UNIQUE_DEVICE_ID = UIDevice.currentDevice().identifierForVendor!.UUIDString
        webServiceURLRequest.setValue(BLUESTREAM_UNIQUE_DEVICE_ID, forHTTPHeaderField: "X-UserAuth-DeviceId")
        //println("unique id: \(BLUESTREAM_UNIQUE_DEVICE_ID)")

        webServiceURLRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")

        // Create body data
        var requestNameValuePairs = [String]()

        let webServiceActionName = "getProjFiles"

        //let siimUserID = txtUserName.text!
        //let siimPassword = txtUserPassword.text!

        requestNameValuePairs.append("action=" + webServiceActionName)
        requestNameValuePairs.append("deviceID=" + BLUESTREAM_UNIQUE_DEVICE_ID)
        requestNameValuePairs.append("projID=" + projID)
        //requestNameValuePairs.append("siimUserID=" + siimUserID)
        //requestNameValuePairs.append("siimPwd=" + siimPassword)

        let requestParametersStr = requestNameValuePairs.joinWithSeparator("&")

        //print("req Para String: \(requestParametersStr)")
        //[action=logIn&deviceID=9684AEB7-73CD-4AAF-95E8-6F2AC9426E04&siimUserID=SM102740&siimPwd=exWahoo77]

        let bodyData = requestParametersStr.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!

        // Test
        //let bodyDataStr = NSString(data: bodyData, encoding: NSUTF8StringEncoding)!
        //print("request body: \(bodyDataStr)")
        //[action=logIn&deviceID=9684AEB7-73CD-4AAF-95E8-6F2AC9426E04&siimUserID=SM102740&siimPwd=exWahoo77]

        webServiceURLRequest.setValue(String(bodyData.length), forHTTPHeaderField: "Content-Length")
        webServiceURLRequest.HTTPBody = bodyData

        var webServiceResponse:NSURLResponse?
        var webServiceError:NSError?

        let beginLoad = NSDate()

        var webServiceData: NSData?

        do
        {
            webServiceData = try NSURLConnection.sendSynchronousRequest(webServiceURLRequest, returningResponse: &webServiceResponse)
        } catch let error as NSError
        {
            webServiceError = error
            webServiceData = nil
        }

        //---------------------
        let endLoad = NSDate()

        /*NSLog(@"Web service [%@] loaded [%lu] bytes data in [%f] seconds, response [%@], response error [%@]", [[self class] description], (unsigned long)[webServiceData length], [endLoad timeIntervalSinceDate:begin], [webServiceResponse description], [webServiceError localizedDescription]);
        NSString *jsonResponseStringForDebug = [[NSString alloc] initWithData:webServiceData encoding:NSUTF8StringEncoding];
        NSLog(@"Web service class [%@], JSON response [%@]", [[self class] description], jsonResponseStringForDebug);
        [jsonResponseStringForDebug release], jsonResponseStringForDebug = nil;*/

        print("---------- Get proj mop files ----------------")
        if webServiceData != nil
        {
            print("Loaded nbr of bytes = \(webServiceData!.length)")
            print("Load time = \(endLoad.timeIntervalSinceDate(beginLoad))")
            print("Response = \(webServiceResponse!.description)")
        }
        //print("Loaded nbr of bytes = \(webServiceData!.length)")
        //print("Load time = \(endLoad.timeIntervalSinceDate(beginLoad))")
        //print("Response = \(webServiceResponse!.description)")
        print("Response Error = \(webServiceError?.localizedDescription)")

        let jsonResponseStringForGetFiles = NSString(data: webServiceData!, encoding: NSUTF8StringEncoding)!
        print("JSON Response for Get Proj Files: \(jsonResponseStringForGetFiles)")

        // Get substring from json response (allows longer response to be appended)
        //let responseString = jsonResponseStringForGetFiles.substringToIndex(16)
        let responseString:String = jsonResponseStringForGetFiles as String
        //print("substring = \(responseString)")

        //if jsonResponseStringForLogOut == "Successful logout"
        //if responseString == "Successful logout"
        //{
        //let alert = UIAlertView.init(title: "Logout Result", message: "Successfully logged out", delegate: nil, cancelButtonTitle: "OK")
        //alert.show()
        //}
        
        ////////////////////////

        // Strip out file extension

        return responseString
    }
}
