//
//  StepDescViewController.swift
//  Call Track
//
//  Created by Biggs, David on 10/26/15.
//  Copyright © 2015 KGP Logistics. All rights reserved.
//

import UIKit

class StepDescViewController: UIViewController
{
    var stepDescText:String?

    @IBOutlet weak var txtViewStepDesc: UITextView!

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtViewStepDesc.text = stepDescText
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
